# TITLE

## Description

JavaScript game template, using HTML 5 canvas.

Can be used as a base for a simple JS game.

Originally based on a JS tutorial from "Franks laboratory" (links below).


**TODO:** should become kind of "engine"/library for simple 2d games.


## Usage

Enter "index.html" path in browser address bar.


## Links

* Original [video tutorial](https://youtu.be/EYf_JwzwTlQ)
* ["Franks laboratory" YouTube channel](https://www.youtube.com/@Frankslaboratory)
